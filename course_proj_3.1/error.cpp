#include "error.h"

Error::Error(std::string message, int code) {
    this->message = message;
    this->code = code;
}

std::string Error::getMessage() {
    return message;
}

int Error::getCode() {
    return code;
}