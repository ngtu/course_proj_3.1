#pragma once
#include <ctime>
#include <iostream>
#include <fstream>
#include <cstring>
#include "category.h"
#include "date.h"
#include "error.h"

class Advert
{
private:
	Date*       date;
	Category*   category;

	char*       phone;
	char*       text;

public:
	Advert();
	Advert(Date date, const char phone[11], Category category, const char* text);
	Advert(Advert& advert);

	~Advert();

	Date* getDate();
	const char* getPhone();
	Category* getCategory();
	const char* getText();

	Advert* setDate(Date date);
	Advert* setCategory(Category category);
	Advert* setPhone(const char phone[11]);
	Advert* setText(const char* text);

	friend std::ostream& operator << (std::ostream& stream, Advert& advert);
	std::istream& operator >> (std::istream& stream);

	friend bool operator < (Advert a1, Advert a2);
	friend bool operator > (Advert a1, Advert a2);
	friend bool operator == (Advert a1, Advert a2);

	Advert* binWrite(std::fstream& stream);	// Запись объекта в бинарный файл
	Advert* binRead(std::fstream& stream);	// Чтение объекта из бинарного файла
};