#pragma once
#include <iostream>

class Date
{
private:
	int year;
	int month;
	int day;

	int getDaysInMonth(int year, int month);
	bool isLeapYear(int year);
	void validateDate(int year, int month, int day);

public:
	Date();
	Date(int year, int month, int day);
	Date(const Date& date);

	int getYear();
	int getMonth();
	int getDay();

	Date* setDate(int year, int month, int day);
	Date* setYear(int year);
	Date* setMonth(int month);
	Date* setDay(int day);

	Date& operator = (Date date);
	friend bool operator == (Date& date1, Date& date2);
	friend bool operator < (Date& date1, Date& date2);
	friend bool operator > (Date& date1, Date& date2);

	friend std::ostream& operator << (std::ostream& os, Date& date);
	friend std::istream& operator >> (std::istream& is, Date& date);

	Date* binWrite(std::fstream& stream);	// Запись объекта в бинарный файл
	Date* binRead(std::fstream& stream);	// Чтение объекта из бинарного файла
};