#pragma once
#include <string>

// Класс ошибки выполения программы
class Error
{
private:
	std::string message;
	int code;

public:
	Error(std::string message, int code);

	std::string getMessage();
	int getCode();
};

