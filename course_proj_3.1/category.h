#pragma once

// Перечисление категорий объявлений
enum Category
{
	NONE, SELL_BUY, TRANSPORT, PROPERTY, MATERIALS
};

const char* categoryToStr(Category* category);	// Конвертирует категорию в строку
int categoryToInt(Category* category);			// Конвертирует категорию в числовой код
Category toCategory(int number);				// Конвертирует числовой код в категорию