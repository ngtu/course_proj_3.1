#include "application.h"
#include <ctime>
#include <chrono>

Application::Application()
{
    this->list = new List<Advert>();

    this->list
        ->pushFront(new Advert(Date(2023, 04, 04), "79994444444", toCategory(4), "This is 4th short text"))
        ->pushFront(new Advert(Date(2023, 02, 02), "79992222222", toCategory(1), "This is 2nd long text"))
        ->pushFront(new Advert(Date(2023, 01, 01), "79991111111", toCategory(1), "This is 1st text"))
        ->pushFront(new Advert(Date(2023, 03, 03), "79993333333", toCategory(3), "This is 3rd text"));
}

Application::~Application()
{
    list->empty();
    delete this->list;
}

void Application::launch()
{
    int item;

    do {
        system(CLEAR);
        printList();

        std::cout
            << "\n***Main menu***\n"
            << "1) Add element\n"
            << "2) Remove element\n"
            << "3) Sort list\n"
            << "4) File\n"
            << "5) Search\n"
            << "6) Exit\n"
            << "\n@: ";
        std::cin >> item;

        switch (item) {
        case 1: this->add(); break;
        case 2: this->remove(); break;
        case 3: this->sort(); break;
        case 4: this->file(); break;
        case 5: this->search(); break;
        default: return;
        }
    } while (item != 6);
}

void Application::printList()
{
    std::cout << "________List________\n";
    this->list->print();
    std::cout << "____________________\n";
}

void Application::add()
{
    system(CLEAR);
    printList();

    std::cout
        << "\n***Add element***\n"
        << "1) First\n"
        << "2) Last\n"
        << "3) N-positioned\n"
        << "4) <- Back\n"
        << "\n@: ";

    int item;
    std::cin >> item;

    switch (item) {
    case 1: this->addFirst(); break;
    case 2: this->addLast(); break;
    case 3: this->addN(); break;
    default: return;
    }
}

void Application::addFirst()
{
    system(CLEAR);
    printList();

    Advert* adv = new Advert();
    adv->operator>>(std::cin);
    this->list->pushFront(adv);
}

void Application::addLast()
{
    system(CLEAR);
    printList();

    Advert* adv = new Advert();
    adv->operator>>(std::cin);
    this->list->pushBack(adv);
}

void Application::addN()
{
    system(CLEAR);
    printList();

    Advert* adv = new Advert();
    adv->operator>>(std::cin);
    int position;
    std::cout << "Insert the position: ";
    std::cin >> position;
    this->list->push(adv, position - 1);
}

void Application::remove()
{
    system(CLEAR);
    printList();

    std::cout
        << "\n***Remove element***\n"
        << "1) First\n"
        << "2) Last\n"
        << "3) N-positioned\n"
        << "4) All\n"
        << "5) <- Back\n"
        << "\n@: ";

    int item;
    std::cin >> item;

    switch (item) {
    case 1: this->removeFirst(); break;
    case 2: this->removeLast(); break;
    case 3: this->removeN(); break;
    case 4: this->removeAll(); break;
    default: return;
    }
}

void Application::removeFirst()
{
    system(CLEAR);
    printList();

    char answ;
    std::cout << "Are you sure? (y - yes, n - no): ";
    std::cin >> answ;

    if (answ == 'y') list->popFront();
}

void Application::removeLast()
{
    system(CLEAR);
    printList();

    char answ;
    std::cout << "Are you sure? (y - yes, n - no): ";
    std::cin >> answ;

    if (answ == 'y') list->popBack();
}

void Application::removeN()
{
    system(CLEAR);
    printList();

    int position;
    std::cout << "Insert the position: ";
    std::cin >> position;

    char answ;
    std::cout << "Are you sure? (y - yes, n - no): ";
    std::cin >> answ;

    if (answ == 'y') list->pop(position - 1);
}

void Application::removeAll()
{
    system(CLEAR);
    printList();

    char answ;
    std::cout << "Are you REALLY SURE? (y - yes, n - no): ";
    std::cin >> answ;

    if (answ == 'y') list->empty();
}

void Application::sort()
{
    system(CLEAR);
    printList();

    std::cout
        << "\n***Sort list***\n"
        << "sorting...";

    this->list->sort();

    std::cout << "[OK]\n";
    system("pause");
}

void Application::file()
{
    system(CLEAR);
    printList();

    std::cout
        << "\n***File***\n"
        << "1) Save\n"
        << "2) Load\n"
        << "3) <- Back\n"
        << "\n@: ";

    int item;
    std::cin >> item;

    switch (item) {
    case 1: this->fileSave(); break;
    case 2: this->fileLoad(); break;
    default: return;
    }
}

void Application::fileSave()
{
    system(CLEAR);
    printList();

    std::cout
        << "\n***File save***\n"
        << "saving...";

    std::fstream file;
    file.open("file.bin");
    this->list->binWrite(file);
    file.close();

    std::cout << "[OK]\n";
    system("pause");
}

void Application::fileLoad()
{
    system(CLEAR);
    printList();

    std::cout
        << "\n***File open***\n"
        << "opening...";

    std::fstream file;
    file.open("file.bin");
    this->list->empty()->binRead(file);
    file.close();

    std::cout << "[OK]\n";
    system("pause");
}

void Application::search()
{
    system(CLEAR);
    printList();

    std::cout
        << "\n***Search***\n"
        << "1) By category\n"
        << "2) By text\n"
        << "3) <- Back\n"
        << "\n@: ";

    int item;
    std::cin >> item;

    switch (item) {
    case 1: this->searchCat(); break;
    case 2: this->searchText(); break;
    default: return;
    }
}

void Application::searchCat()
{
    system(CLEAR);

    int cat;
    std::cout << "Insert category: 1)Sell, Buy 2)Transport 3)Property 4)Materials 0)None: ";
    std::cin >> cat;
    
    std::cout << "\n_____Found_nodes____\n";
    List<Advert>* result = this->list
        ->search(toCategory(cat))
        ->print()
        ->empty();
    delete result;
    std::cout << "____________________\n";
    system("pause");
}

void Application::searchText()
{
    system(CLEAR);
    
    char text[255];
    std::cout << "Insert text: ";
    std::cin.ignore();
    std::cin.getline(text, sizeof(text));
    
    std::cout << "\n_____Found_nodes____\n";
    List<Advert>* result = this->list
        ->search(text)
        ->print()
        ->empty();
    delete result;
    std::cout << "____________________\n";
    system("pause");
}

/// <summary>
/// Возвращает псевдослучайное число от min до max
/// </summary>
/// <param name="min">Минимальное значение</param>
/// <param name="max">Максимальное значение</param>
/// <returns></returns>
int randomInt(int min, int max)
{
    return min + rand() % (max - min + 1);
}

/// <summary>
/// Автоматическое тестирование
/// </summary>
/// <param name="count">Количество элементов списка</param>
void Application::test(int count)
{
    srand(time(NULL));

    List<int>* intList = new List<int>();
    for (int i = 0; i < count; i++) {
        intList->pushBack(new int(randomInt(1, 100)));
    }
    std::cout << "***Testing List<int>***\n"
        << "Sorting " << count << " elements... ";
    auto startTime = std::chrono::steady_clock::now();
    intList->sort();
    auto endTime = std::chrono::steady_clock::now();
    auto elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime);
    std::cout << "duration: " << elapsed_ms.count() << " ms\n";

    List<double>* dblList = new List<double>();
    for (int i = 0; i < count; i++) {
        dblList->pushBack(new double(randomInt(1, 100) * 0.1));
    }
    std::cout << "***Testing List<double>***\n"
        << "Sorting " << count << " elements... ";
    startTime = std::chrono::steady_clock::now();
    dblList->sort();
    endTime = std::chrono::steady_clock::now();
    elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime);
    std::cout << "duration: " << elapsed_ms.count() << " ms\n";
}