#include "advert.h"

Advert::Advert()
{
	this->date = new Date();
	this->category = new Category();
	this->phone = new char[12];
	this->text = new char[1];
}

Advert::Advert(Date date, const char phone[11], Category category, const char* text)
{
	this->date = new Date();
	this->category = new Category();
	this->phone = new char[12];

    this->setDate(date);
    this->setCategory(category);
    this->setPhone(phone);
    this->setText(text);
}

Advert::Advert(Advert& advert)
{
	this->date = new Date();
	this->category = new Category();
	this->phone = new char[12];

    this->setDate(*advert.getDate());
    this->setCategory(*advert.getCategory());
    this->setPhone(advert.getPhone());
    this->setText(advert.getText());
}

Advert::~Advert()
{
	delete this->date;
	delete this->category;
	delete[] this->phone;
	delete[] this->text;
}

Date *Advert::getDate() {
    return this->date;
}

const char *Advert::getPhone() {
    return this->phone;
}

Category *Advert::getCategory() {
    return this->category;
}

const char *Advert::getText() {
    return this->text;
}

Advert* Advert::setDate(Date date)
{
	*this->date = date;
	return this;
}

Advert* Advert::setCategory(Category category)
{
	*this->category = category;
	return this;
}

Advert* Advert::setPhone(const char phone[11])
{
	strcpy(this->phone, phone);
	return this;
}

Advert* Advert::setText(const char* text)
{
	this->text = new char[strlen(text) + 1];
	strcpy(this->text, text);
	return this;
}

Advert* Advert::binWrite(std::fstream& stream)
{
	if (!stream.is_open()) throw Error("File is not open", -4);

	// Запись полей объекта
	this->date->binWrite(stream);

	// Запись текстового поля
	int length = strlen(this->phone);				// Вычисление длины строки
	stream.write((char*)&length, sizeof(length));	// Запись длины строки
	stream.write(this->phone, length);				// Запись строки

	int catId = categoryToInt(this->category);
	stream.write((char*)&catId, sizeof(catId));

	length = strlen(this->text);
	stream.write((char*)&length, sizeof(length));
	stream.write(this->text, length);

	return this;
}

Advert* Advert::binRead(std::fstream& stream)
{
	if (!stream.is_open()) throw Error("File is not open", -4);

	// Чтение полей объекта
	this->date->binRead(stream);

	int length = 0;

	// Чтение текстового поля
	delete[] this->phone;							// Очистка строки
	stream.read((char*)&length, sizeof(length));	// Чтение длины строки
	this->phone = new char[length + 1];				// Выделение памяти под строку
	stream.read(this->phone, length);				// Чтение строки
	this->phone[length] = '\0';						// Добавление символа конца строки

	int catId = 0;
	stream.read((char*)&catId, sizeof(catId));
	this->setCategory(toCategory(catId));

	delete[] this->text;
	stream.read((char*)&length, sizeof(length));
	this->text = new char[length + 1];
	stream.read(this->text, length);
	this->text[length] = '\0';

	return this;
}

std::ostream& operator<<(std::ostream& stream, Advert& advert)
{
	stream
		<< "Date: "         << *advert.date
		<< "\nPhone: +"     << advert.getPhone()
		<< "\nCategory: "   << categoryToStr(advert.getCategory())
		<< "\nText: "       << advert.getText() << "\n";

	return stream;
}

std::istream& Advert::operator>>(std::istream& stream)
{
	Date date;
	stream >> date;
	this->setDate(date);

	char phone[12];
	std::cout << "Insert phone: ";
	stream >> phone;
	this->setPhone(phone);

	int cat;
	std::cout << "Insert category: 1)Sell, Buy 2)Transport 3)Property 4)Materials 0)None: ";
	stream >> cat;
	this->setCategory(toCategory(cat));

	char text[255];
	std::cout << "Insert text: ";
	stream.ignore();
	stream.getline(text, sizeof(text));
	this->setText(text);

	return stream;
}

bool operator<(Advert a1, Advert a2)
{
	return *a1.getDate() < *a2.getDate();
}

bool operator>(Advert a1, Advert a2)
{
	return *a1.getDate() > *a2.getDate();
}

bool operator==(Advert a1, Advert a2)
{
	return *a1.getDate() == *a2.getDate();
}