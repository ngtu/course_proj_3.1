#pragma once
#include <iostream>
#include <cstdlib>
#include "list.h"
#include "advert.h"
#include "error.h"
#include "date.h"

#ifdef _WIN32
#define CLEAR "cls"
#else //In any other OS
#define CLEAR "clear"
#endif

class Application
{
private:
	List<Advert>* list;

public:
	Application();
	~Application();

	void launch();			// Запуск программы
	void test(int count);	// Автоматическое тестирование

private:
	void printList();		// Вывод списка на экран
	
	void add();				// Добавление элемента
	void addFirst();
	void addLast();
	void addN();

	void remove();			// Удаление элемента
	void removeFirst();
	void removeLast();
	void removeN();
	void removeAll();

	void sort();			// Сортировка

	void file();			// Работа с бинарным файлом
	void fileSave();
	void fileLoad();

	void search();			// Поиск по списку
	void searchCat();
	void searchText();
};

